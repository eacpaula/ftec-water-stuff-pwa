import React from 'react'
import { BrowserRouter, Switch } from 'react-router-dom'

import Login from '../containers/Login'
import ResetPassword from '../containers/ResetPassword'
import Dashboard from '../containers/Dashboard'
import Register from '../containers/Register'
import Water from '../containers/Water'
import WaterReading from '../containers/WaterReading'
import WaterReinstallation from '../containers/WaterReinstallation'
import WaterUnstallation from '../containers/WaterUnstallation'
import Invoice from '../containers/Invoice'
import InvoiceOnlinePayment from '../containers/InvoiceOnlinePayment'
import InvoiceAutomicDebit from '../containers/InvoiceAutomicDebit'
import Profile from '../containers/Profile'
import Ratings from '../containers/Ratings'
import RatingDetail from '../containers/Ratings/View'
import Complaints from '../containers/Complaints'
import ComplaintDetail from '../containers/Complaints/View'
import PublicNotes from '../containers/PublicNotes'
import PublicNoteDetail from '../containers/PublicNotes/View'

import PrivateRoute from './privateRoute'
import PublicRoute from './publicRoute'

const Router = () => (
	<BrowserRouter>
		<Switch>
			<PublicRoute component={Login} path={'/'} exact />
			<PublicRoute component={Login} path={'/login'} />
			<PublicRoute component={Register} path={'/register'} />
			<PublicRoute component={ResetPassword} path={'/reset-password'} />
			<PrivateRoute component={Dashboard} path={'/dashboard'} />
			<PrivateRoute component={Water} path={'/water'} />
			<PrivateRoute component={WaterReading} path={'/water-reading'} />
			<PrivateRoute component={WaterReinstallation} path={'/water-reinstalation'} />
			<PrivateRoute component={WaterUnstallation} path={'/water-unstallation'} />
			<PrivateRoute component={Invoice} path={'/invoice'} />
			<PrivateRoute component={InvoiceOnlinePayment} path={'/invoice-online-payment'} />
			<PrivateRoute component={InvoiceAutomicDebit} path={'/invoice-automatic-debit'} />
			<PrivateRoute component={Profile} path={'/profile'} />
			<PrivateRoute component={RatingDetail} path={'/ratings/:id'} />
			<PrivateRoute component={Ratings} path={'/ratings'} />
			<PrivateRoute component={ComplaintDetail} path={'/complaints/:id'} />
			<PrivateRoute component={Complaints} path={'/complaints'} />
			<PrivateRoute component={PublicNoteDetail} path={'/public-notes/:id'} />
			<PrivateRoute component={PublicNotes} path={'/public-notes'} />
		</Switch>
	</BrowserRouter>
)

export default Router