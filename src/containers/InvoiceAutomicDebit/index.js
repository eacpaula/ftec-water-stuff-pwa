import React, { useState } from 'react'
import { CircularProgress } from '@material-ui/core'
import { useQuery } from '@apollo/client'
import { object, string, number } from "yup"

import queries from './queries'
import mutations from './mutations'

import Content from '../../components/Content'
import Form from '../../components/Form'

export default function InvoiceAutomicDebit() {
  const [ enableForm , setEnableForm ] = useState(false)
  const [ responseMessage , setResponseMessage ] = useState("")

  const initialState = {
    code: "",
    address: "",
    message: ""
  }

  const schema = object().shape({
    bank: string().required("O banco é obrigatório!"),
    agency: string().required("A agência é obrigatória!"),
    account: string().required("A conta é obrigatória!"),
    account_digit: string().required("O dígito da conta é obrigatório!"),
    fullname: string().required("O nome completo é obrigatório!"),
    type: number().required("O tipo de documento é obrigatório!"),
    documentation: string().required("O documento é obrigatório!"),
    cellphone: string().required("O celular é obrigatório!"),
    email: string().required("O email é obrigatório!")
	})

  const inputs = [
    {
      type: "text",
      name: "bank",
      label: "Banco",
      props: {
        placeholder: "Banco",
        style: ""
      }
    },
    {
      type: "text",
      name: "agency",
      label: "Agência",
      props: {
        placeholder: "Agência",
        style: ""
      }
    },
    {
      type: "text",
      name: "account",
      label: "Conta",
      props: {
        placeholder: "Conta",
        style: ""
      }
    },
    {
      type: "text",
      name: "account_digit",
      label: "Dígito",
      props: {
        placeholder: "Dígito",
        style: ""
      }
    },
    {
      type: "text",
      name: "fullname",
      label: "Titular",
      props: {
        placeholder: "Titular",
      }
    },
    {
      type: "radio-group",
      name: "type",
      label: "Tipo de Documento",
      defaultValue: "1",
      props: { },
      options: [
        { label: "CPF", value: "1"},
        { label: "CNPJ", value: "2"}
      ]
    },
    {
      type: "text",
      name: "documentation",
      label: "RG",
      props: {
        placeholder: "RG",
      }
    },
    {
      type: "text",
      name: "cellphone",
      label: "Celular",
      props: {
        placeholder: "Celular",
      }
    },
    {
      type: "text",
      name: "email",
      label: "Email",
      props: {
        placeholder: "Email",
      }
    }
  ]

  const { loading } = useQuery( queries.GET_DATA, {
    onCompleted: (response) => {
      var data
      
      if(response && response.checkStatusAutomaticDebit) {
        const info = response.checkStatusAutomaticDebit;
        
        if(info.success && info.enableRequest) {
          setEnableForm(true)
        }

        setResponseMessage(info.message)
      }
    },
		onError: (error) => {
			console.log(error)
		}
  })

  const [state, setState] = useState({
    inputs, 
    schema, 
    initialState,
    data: {}
  })

  const mutation = { definition: mutations.ADD_DATA, name: 'automaticDebit' }

  return (
    <Content title={'Cadastro de débito automático'}>
      { loading ?
          <CircularProgress disableShrink />
        :
          enableForm ? 
            <Form 
              title="Solicitação de Débito Automático" 
              description=""
              source={state.data} 
              inputs={state.inputs} 
              schema={state.schema} 
              mutation={mutation}
              callback={(success, data) => {}}
              initialState={state.initialState} />
              :
            <div>
              <p>{responseMessage}</p>
            </div>
      } 
    </Content>
  )
}
