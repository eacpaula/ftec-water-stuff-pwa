import { gql } from '@apollo/client'

const GET_DATA = gql`
  query {
    checkStatusAutomaticDebit {
      enableRequest
      success
      status
      message
    }
  }
`;

const queries = {
  GET_DATA
}

export default queries