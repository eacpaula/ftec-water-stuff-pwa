import { gql } from '@apollo/client'

const ADD_DATA = gql`
  mutation AutomaticDebit(
    $account: String!,
    $account_digit: String!,
    $agency: String!,
    $bank: String!,
    $cellphone: String!,
    $documentation: String!,
    $fullname: String!,
    $email: String!
  ) {
    automaticDebit(
      data: {
        account: $account,
        account_digit: $account_digit,
        agency: $agency,
        bank: $bank,
        cellphone: $cellphone,
        documentation: $documentation,
        fullname: $fullname,
        email: $email,
      }
    ) {
      success
      message
    }
  }
`;

const mutations = {
  ADD_DATA
}

export default mutations