import { React } from 'react'
import moment from 'moment'
import { useLocation, useHistory } from 'react-router'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'

import Content from '../../components/Content'

import './styles.css'


export default function PublicNote() {
  const history = useHistory()
  const location = useLocation()
  const data = location.state.data

  const handleClick = () => {
    history.goBack()
  }

  return (
    <Content title={'Listagem de Avaliações'}>
      <div className='news-header'>
        <button onClick={handleClick}>
          <ArrowBackIcon className='news-header-back-history' />
        </button>
        <h3>Avaliação referente ao contato do dia {moment(data.contact.createdAt).format('DD/MM/YYYY')}</h3>
      </div>
      <section>
        <p>Nota: {data.note}</p>
        <p>Contato do dia: {moment(data.contact.createdAt).format('DD/MM/YYYY')}</p>
        <p>Assunto do contato: {data.contact.subject.title}</p>
        <p>Mensagem encaminhada: {data.contact.message}</p>
        <p>
          Observação da avaliação: 
          <div dangerouslySetInnerHTML={{ __html: data.observation }} />
        </p>
      </section>
    </Content>
  )
}