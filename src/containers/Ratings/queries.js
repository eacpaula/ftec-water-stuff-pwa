import { gql } from '@apollo/client'

const GET_DATA = gql`
  query {
    ratingsByUser(params: { term: "" }) {
      id
      note
      observation
      createdAt
      contact {
        message
        createdAt
        subject {
          title
        }
      }
      createdBy {
        id
        email
      }
    }
  }
`;

const GET_AVAILABLE_CONTACTS_TO_RATING = gql`
  query {
    contactsByUserToRating(params: { term: "" }) {
      id
      fullname
      email
      cellphone
      message
      createdAt
      subject {
        title
      }
    }
  }
`;


const queries = {
  GET_DATA,
  GET_AVAILABLE_CONTACTS_TO_RATING
}

export default queries