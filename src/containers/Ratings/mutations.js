import { gql } from '@apollo/client'

const ADD_DATA = gql`
  mutation AddRating(
    $note: Int!,
    $observation: String!
    $contact_id: Int!, 
  ) {
    addRating(
      data: {
        note: $note,
        observation: $observation,
        contact_id: $contact_id
      }
    ) {
      id
    }
  }
`;

const mutations = {
  ADD_DATA
}

export default mutations