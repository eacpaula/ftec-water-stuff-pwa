import React, { useState, useEffect,  Fragment } from 'react'
import moment from 'moment'
import { CircularProgress } from '@material-ui/core'
import { useLazyQuery } from '@apollo/client'
import { object, string, number } from "yup"

import queries from './queries'
import mutations from './mutations'

import Content from '../../components/Content'
import List from '../../components/List'

export default function RegisterRatings() {
  const [contacts, setContacts] = useState([]);
  const [loading, setLoading] = useState([]);

  const initialState = {
    note: 0,
    observation: "",
    contact_id: null
  }

  const schema = object().shape({
    note: number(),
    observation: string(),
    contact_id: number().required("É obrigatório a seleção de um contato para atribuir uma nota ao atendimento!"),
	})

  const [inputs, setInputs] = useState([
    {
      type: "text",
      name: "note",
      label: "Nota",
      props: {
        placeholder: "Nota",
      }
    },
    {
      type: "editor",
      name: "observation",
      label: "Observação",
      objectName: 'observation',
      props: {
        placeholder: "Observação",
      }
    },
    {
      type: "select",
      name: "contact_id",
      label: "Contato Avaliado",
      props: {
        placeholder: "Nenhum contato selecionado para avaliação",
      },
      options: contacts && contacts.length > 0 ? contacts.map(x => { return { value: x.id, label: x.title } }) : []
    },
  ])

  const [loadContacts] = useLazyQuery(queries.GET_AVAILABLE_CONTACTS_TO_RATING, {
    fetchPolicy: 'no-cache',
    onCompleted: (data) => {
      setContacts(data.contactsByUserToRating)

      let newInputs = [ ...inputs ]
      
      newInputs[2].options = data.contactsByUserToRating.map(x => { 
        return { 
          value: x.id, 
          label: `Contato realizado dia "${moment(x.createdAt).format('DD/MM/YYYY')}" com o assunto "${x.subject.title}"` 
        }
      }) || []

      setInputs(newInputs)
      setLoading(false)
    },
		onError: (error) => {
			console.log(error)
      setLoading(false)
		}
  })

  const [loadRatings] = useLazyQuery(queries.GET_DATA, {
    fetchPolicy: 'no-cache',
    onCompleted: (response) => {
      var data = []

      if(response && response.ratingsByUser) {
        const info = response.ratingsByUser;

        for (const row of info) {
          data.push({
            ...row,
            contact_createdAt: row.contact.createdAt,
            contact_subject: row.contact.subject.title,
            contact_message: row.contact.message,
            path: 'ratings'
          })
        }
      }

      setState({
        ...state,
        data
      })
    },
		onError: (error) => {
			console.log(error)
		}
  })

  const handleAdd = (result) => {
    if(result.success) {
      setLoading(true)
      loadRatings()
      loadContacts()
    }
  }

  const [state, setState] = useState({
    columns: [
      { title: 'Nota', field: 'note'},
      { title: 'Dia da Avaliação', field: 'createdAt', render: rowData => <Fragment> { moment(rowData.createdAt).format('DD/MM/YYYY') } </Fragment> },
      { title: 'Dia do Contato', field: 'contact_createdAt', render: rowData => <Fragment> { moment(rowData.contact_createdAt).format('DD/MM/YYYY') } </Fragment> },
      { title: 'Assunto', field: 'contact_subject'},
    ],
    data: [],
    actions: [
      { 
        action: 'view', 
        title: "Visualizar Avaliação"
      },
      { 
        action: 'add', 
        title: "Cadastrar Avaliação", 
        callback: handleAdd, 
        mutation: { definition: mutations.ADD_DATA, name: 'addRating' }, 
        inputs, 
        schema, 
        initialState 
      }
    ] 
  })

  useEffect(() => {
		if(state.data.length <= 0){
      setLoading(true)
      loadRatings()
    }

    if(contacts && contacts.length <= 0) {
      loadContacts()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
	}, [state])

  return (
    <Content title={'Avaliações'}>
      { loading ?
          <CircularProgress disableShrink />
        :
          <List
            title={'Avaliações Cadastradas'}
            columns={state.columns} 
            rows={state.data}
            actions={state.actions}
          />
      }
    </Content>
  )
}