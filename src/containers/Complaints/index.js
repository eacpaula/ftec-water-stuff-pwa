import React, { useState, useEffect,  Fragment } from 'react'
import moment from 'moment'
import { CircularProgress } from '@material-ui/core'
import { useLazyQuery } from '@apollo/client'
import { object, string } from "yup"

import queries from './queries'
import mutations from './mutations'

import Content from '../../components/Content'
import List from '../../components/List'

export default function Complaints() {
  const [loading, setLoading] = useState([]);

  const initialState = {
    title: "",
    subtitle: "",
    description: ""
  }

  const schema = object().shape({
    title: string().required("O título é um campo obrigatório"),
    subtitle: string().required("O subtítulo é um campo obrigatório"),
    description: string().required("A descrição é um campo obrigatório"),
	})

  const inputs = [
    {
      type: "text",
      name: "title",
      label: "Título",
      props: {
        placeholder: "Título",
      }
    },
    {
      type: "text",
      name: "subtitle",
      label: "Subtítulo",
      props: {
        placeholder: "Subtítulo",
      }
    },

    {
      type: "editor",
      name: "description",
      label: "Descrição",
      objectName: 'description',
      props: {
        placeholder: "Descrição",
      }
    },
  ]

  const [loadComplaints] = useLazyQuery(queries.GET_DATA, {
    fetchPolicy: 'no-cache',
    onCompleted: (data) => {
      setState({ 
        ...state, 
        data: data.complaintsByUser.map(x => Object.assign({}, { ...x, path: 'complaints' }))
      })
      setLoading(false)
    },
		onError: (error) => {
			console.log(error)
      setLoading(false)
		}
  })

  const handleAdd = (result) => {
    if(result.success) {
      setLoading(true)
      loadComplaints()
    }
  }

  const [state, setState] = useState({
    columns: [
      { title: 'Título', field: 'title'},
      { title: 'Título', field: 'subtitle'},
      { title: 'Dia da Denúncia', field: 'createdAt', render: rowData => <Fragment> { moment(rowData.createdAt).format('DD/MM/YYYY') } </Fragment> },
    ],
    data: null,
    actions: [
      { 
        action: 'view', 
        title: "Visualizar Denúncia"
      },
      { 
        action: 'add', 
        title: "Cadastrar Denúncia", 
        callback: handleAdd, 
        mutation: { definition: mutations.ADD_DATA, name: 'addComplaint' }, 
        inputs, 
        schema, 
        initialState 
      }
    ] 
  })

  useEffect(() => {
		(async () => {
      if(!state.data){
        setLoading(true)
			  loadComplaints()
      }
    })()
    // eslint-disable-next-line react-hooks/exhaustive-deps
	}, [state])

  return (
    <Content title={'Denúncias'}>
      { loading ?
          <CircularProgress disableShrink />
        :
          <List
            title={'Denúncias Cadastradas'}
            columns={state.columns} 
            rows={state.data}
            actions={state.actions}
          />
      }
    </Content>
  )
}