import { gql } from '@apollo/client'

const GET_DATA = gql`
  query {
    complaintsByUser(params: { term: "" }) {
      id
      title
      subtitle
      description
      createdBy {
        id
        email
      }
    }
  }
`;

const queries = {
  GET_DATA
}

export default queries