import { React } from 'react'
import { useLocation, useHistory } from 'react-router'
import { Icon } from '@material-ui/core'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'

import Content from '../../components/Content'

import './styles.css'


export default function PublicNote() {
  const history = useHistory()
  const location = useLocation()
  const data = location.state.data

  const handleClick = () => {
    history.goBack()
  }

  return (
    <Content title={'Denúncias'}>
      <div className='complaint-header'>
        <button onClick={handleClick}>
          <ArrowBackIcon className='complaint-header-back-history' />
        </button>
        <h3>{data.title}</h3>
      </div>
      <section>
        <h5>{data.subtitle}</h5>
        <div dangerouslySetInnerHTML={{ __html: data.description }} />
      </section>
    </Content>
  )
}