import * as React from 'react'
import Content from '../../components/Content'
import { CssBaseline, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
  hide: {
    display: 'none',
  },
  container: {
    width: '80%',
    margin: 'auto',
  },
}))

export default function Dashboard() {
  const classes = useStyles()
  return (
    <Content title={'Dashboard'}>
      <CssBaseline />
      <form>
        <div className={classes.container}>
          <Typography component={'h3'} variant={'h6'}>
            Dashboard
          </Typography>
          <Typography component={'p'}>
            Instalação: 9999999
          </Typography>
          <Typography component={'p'}>
            Endereço: R. Gustavo Ramos Sehbe, 79-223 - Desvio Rizzo, Caxias do Sul - RS, 95012-669
          </Typography>
        </div>
      </form>
    </Content>
  )
}
