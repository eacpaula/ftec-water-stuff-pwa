import { gql } from '@apollo/client'

const GET_DATA = gql`
  query {
    pendantInvoicesByUser(params: { term: "" }) {
      id
      total
      barcode
      status
      createdAt
      waterRegister {
        id
        code
        country
        state
        city
        neighborhood
        address
        zipcode
        cellphone
        telephone
      }
      createdBy {
        username
      }
    }
  }
`;

const queries = {
  GET_DATA
}

export default queries