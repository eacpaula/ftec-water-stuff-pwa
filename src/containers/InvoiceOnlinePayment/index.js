import React, { useState } from 'react'
import { CircularProgress } from '@material-ui/core'
import { useQuery } from '@apollo/client'
import { object, string, number } from "yup"

import queries from './queries'

import Content from '../../components/Content'
import Form from '../../components/Form'

export default function InvoiceAutomicDebit() {
  const initialState = {
    code: "",
    address: "",
    message: ""
  }

  const schema = object().shape({
    code: string(),
    address: string(),
    message: string().required("A mensagem é obrigatória!"),
    water_register_id: number()
	})

  const inputs = [
    {
      type: "text",
      name: "code",
      label: "Código da Conta de Água",
      readOnly: true,
      props: {
        placeholder: "Código",
        style: ""
      }
    },
    {
      type: "text",
      name: "address",
      label: "Endereço",
      readOnly: true,
      props: {
        placeholder: "Endereço",
      }
    },
    {
      type: "text",
      name: "barcode",
      label: "Código de Barras",
      readOnly: true,
      props: {
        placeholder: "Código de Barras",
      }
    }
  ]

  const { loading } = useQuery( queries.GET_DATA, {
    onCompleted: (response) => {
      var data
      
      if(response && response.pendantInvoicesByUser) {
        const info = response.pendantInvoicesByUser;
        
        data = {
          ...info,
          address: `${info.waterRegister.address} - ${info.waterRegister.neighborhood} - ${info.waterRegister.city} - ${info.waterRegister.state} - ${info.waterRegister.zipcode}`
        }
      }

      setState({ 
        ...state,
        data
      })
    },
		onError: (error) => {
			console.log(error)
		}
  })

  const [state, setState] = useState({
    inputs, 
    schema, 
    initialState,
    data: {}
  })

  return (
    <Content title={'Pagamento online'}>
      { loading ?
          <CircularProgress disableShrink />
        :
          <Form 
            title="Pagamento online" 
            description=""
            source={state.data} 
            inputs={state.inputs} 
            schema={state.schema} 
            callback={(success, data) => {}}
            initialState={state.initialState} />
      } 
    </Content>
  )
}
