import moment from 'moment'
import React, { useState, useEffect, Fragment } from 'react'
import { CircularProgress } from '@material-ui/core'
import { useLazyQuery } from '@apollo/client'

import Content from '../../components/Content'
import List from '../../components/List'

import queries from './queries'

export default function WaterReading() {
  const [loading, setLoading] = useState([]);

  const [state, setState] = useState({
    columns: [
      { title: 'Id', field: 'id' },
      { title: 'Mês', field: 'createdAt', render: rowData => <Fragment> { moment(rowData.createdAt).format('MMM/YYYY') } </Fragment> },
      { title: 'Valor', field: 'total', render: rowData => <Fragment> { `R$ ${ rowData.total.toString().replace('.', ',') }` } </Fragment>},
      { title: 'Status', field: 'createdAt', render: rowData => <Fragment> { rowData.status && rowData.status === 0 ? 'A Pagar' : 'Pago' } </Fragment> },
      { title: 'Criado Em', field: 'createdAt', render: rowData => <Fragment> { moment(rowData.createdAt).format('DD/MM/YYYY') } </Fragment> }
    ],
    data: null,
    actions: []
  })

  const [loadInvoices] = useLazyQuery(queries.GET_DATA, {
    fetchPolicy: 'no-cache',
    onCompleted: (data) => {
      setState({ 
        ...state, 
        data: data.invoicesByUser.map(x => Object.assign({}, { ...x }))
      })
      setLoading(false)
    },
		onError: (error) => {
			console.log(error)
      setLoading(false)
		}
  })

  useEffect(() => {
		(async () => {
      if(!state.data){
        setLoading(true)
			  loadInvoices()
      }
    })()
    // eslint-disable-next-line react-hooks/exhaustive-deps
	}, [state])

  return (
    <Content title={'Histórico de faturas'}>
      { loading ?
          <CircularProgress disableShrink />
        :
          <List
            title={'Faturas'}
            columns={state.columns} 
            rows={state.data}
            actions={state.actions}
          />
      }
    </Content>
  )
}
