import { gql } from '@apollo/client'

const GET_DATA = gql`
  query {
    invoicesByUser(params: { term: "" }) {
      id
      total
      barcode
      status
      createdAt
      waterRegister {
        id
        code
      }
      createdBy {
        username
      }
    }
  }
`;

const queries = {
  GET_DATA
}

export default queries