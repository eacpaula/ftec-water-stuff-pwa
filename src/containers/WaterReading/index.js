import moment from 'moment'
import { React, useState, useEffect, Fragment } from 'react'
import { CircularProgress } from '@material-ui/core'
import { useQuery } from '@apollo/client'

import Content from '../../components/Content'
import List from '../../components/List'

import queries from './queries'

export default function WaterReading() {
  const [state, setState] = useState({
    columns: [
      { title: 'Id', field: 'id' },
      { title: 'Código', field: 'code', render: rowData => rowData.waterRegister.code },
      { title: 'Mês', field: 'createdAt', render: rowData => <Fragment> { moment(rowData.createdAt).format('MMM/YYYY') } </Fragment> },
      { title: 'Valor', field: 'comsumption'},
      { title: 'Criado Em', field: 'createdAt', render: rowData => <Fragment> { moment(rowData.createdAt).format('DD/MM/YYYY') } </Fragment> }
    ],
    data: [],
    actions: []
  })

  const { loading } = useQuery(queries.GET_DATA, {
    onCompleted: (data) => {
      setState({ 
        columns: state.columns, 
        data: data.waterRegisterHistoriesByUser.map(x => Object.assign({}, { ...x })), 
        actions: state.actions 
      })
    },
		onError: (error) => {
			console.log(error)
		}
  })

  return (
    <Content title={'Leituras de água'}>
      { loading ?
          <CircularProgress disableShrink />
        :
          <List
            title={'Leituras de água'}
            columns={state.columns} 
            rows={state.data}
            actions={state.actions}
          />
      }
    </Content>
  )
}