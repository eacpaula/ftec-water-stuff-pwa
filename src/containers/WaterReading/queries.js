import { gql } from '@apollo/client'

const GET_DATA = gql`
  query {
    waterRegisterHistoriesByUser(params: { term: "" }) {
      id
      consumption
      createdAt
      waterRegister {
        id
        code
      }
      createdBy {
        username
      }
    }
  }
`;

const queries = {
  GET_DATA
}

export default queries