import { React } from 'react'
import { useLocation, useHistory } from 'react-router'
import { Icon } from '@material-ui/core'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'

import Content from '../../components/Content'

import './styles.css'


export default function PublicNote() {
  const history = useHistory()
  const location = useLocation()
  const data = location.state.data

  const handleClick = () => {
    history.goBack()
  }

  return (
    <Content title={'Listagem de notas públicas'}>
      <div className='news-header'>
        <button onClick={handleClick}>
          <ArrowBackIcon className='news-header-back-history' />
        </button>
        <h3>{data.title}</h3>
      </div>
      <section dangerouslySetInnerHTML={{ __html: data.content }} />
      <section>
        <img src={`${data.media.path}`} style={{width: 50}} alt={data.media.filename_original }/>
      </section>
    </Content>
  )
}