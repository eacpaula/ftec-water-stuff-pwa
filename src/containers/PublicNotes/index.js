import moment from 'moment'
import { useHistory } from 'react-router-dom'
import { React, useState, useEffect, Fragment } from 'react'
import { CircularProgress, Button } from '@material-ui/core'
import { useQuery } from '@apollo/client'

import Content from '../../components/Content'
import List from '../../components/List'

import queries from './queries'

export default function PublicNotes() {
  const [state, setState] = useState({
    columns: [
      { title: 'Título', field: 'title' },
      { title: 'Descrição', field: 'description' },
      { title: 'Imagem', field: 'media', render: rowData => <img src={`${rowData.media.path}`} style={{width: 50}} alt={rowData.media.filename_original }/> },
    ],
    data: [],
    actions: [
      { 
        action: 'view', 
        title: "Visualizar Nota Pública"
      },
    ]
  })

  const { loading } = useQuery(queries.GET_DATA, {
    onCompleted: (data) => {
      setState({
        columns: state.columns,
        data: data.tidings.map(x => Object.assign({}, { ...x, path: 'public-notes' })),
        actions: state.actions
      })
    },
		onError: (error) => {
			console.log(error)
		}
  })

  return (
    <Content title={'Listagem de notas públicas'}>
      { loading ?
          <CircularProgress disableShrink />
        :
          <Fragment>
            <List
              title={'Listagem de notas públicas'}
              columns={state.columns}
              rows={state.data}
              actions={state.actions}
            />
          </Fragment>
      }
    </Content>
  )
}