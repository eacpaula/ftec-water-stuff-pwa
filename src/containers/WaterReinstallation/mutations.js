import { gql } from '@apollo/client'

const ADD_DATA = gql`
  mutation AddWaterRegisterContact(
    $water_register_id: Int!, 
    $message: String!
  ) {
    addWaterRegisterContact(
      data: {
        subject_id: 1,
        message: $message,
        water_register_id: $water_register_id
      }
    ) {
      id
    }
  }
`;

const mutations = {
  ADD_DATA
}

export default mutations