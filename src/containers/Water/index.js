import React, { useState } from 'react'
import Content from '../../components/Content'
import { CircularProgress } from '@material-ui/core'
import { useQuery } from '@apollo/client'
import { object, string, number, boolean } from "yup"

import queries from './queries'
import mutations from './mutations'

import Form from '../../components/Form'

export default function Water() {
  const initialState = {
    code: "",
    documentation: "",
    media_user_id: "",
    media_documentation_frontside_id: "",
    media_documentation_backside_id: "",
    media_water_account_id: "",
    accept_terms: "true",
    available: "",
    country: "",
    state: "",
    city: "",
    neighborhood: "",
    address: "",
    zipcode: "",
    cellphone: "",
    telephone: ""
  }

  const schema = object().shape({
    code: string().required("A imagem da notícia é obrigatória!"),
    documentation: string().required("A imagem da notícia é obrigatória!"),
    media_user_id: number().required("A imagem da notícia é obrigatória!"),
    media_documentation_frontside_id: number().required("A imagem da notícia é obrigatória!"),
    media_documentation_backside_id: number().required("A imagem da notícia é obrigatória!"),
    media_water_account_id: number().required("A imagem da notícia é obrigatória!"),
    accept_terms: boolean().required("A imagem da notícia é obrigatória!"),
    country: string().required("A imagem da notícia é obrigatória!"),
    state: string().required("A imagem da notícia é obrigatória!"),
    city: string().required("A imagem da notícia é obrigatória!"),
    neighborhood: string().required("A imagem da notícia é obrigatória!"),
    address: string().required("A imagem da notícia é obrigatória!"),
    zipcode: string().required("A imagem da notícia é obrigatória!"),
    cellphone: string().required("A imagem da notícia é obrigatória!"),
    telephone: string().required("A imagem da notícia é obrigatória!"),
	})

  const inputs = [
    {
      type: "upload",
      name: "media_user_id",
      objectName: 'mediaUser',
      label: "Foto",
      props: {
        placeholder: "Nenhum arquivo selecionado",
      }
    },
    {
      type: "upload",
      name: "media_documentation_frontside_id",
      objectName: 'mediaDocumentationFrontside',
      label: "Identidade Frente",
      props: {
        placeholder: "Nenhum arquivo selecionado",
      }
    },
    {
      type: "upload",
      name: "media_documentation_backside_id",
      objectName: 'mediaDocumentationBackside',
      label: "Identidade Verso",
      props: {
        placeholder: "Nenhum arquivo selecionado",
      }
    },
    {
      type: "upload",
      name: "media_water_account_id",
      objectName: 'mediaWaterAccount',
      label: "Conta de Água",
      props: {
        placeholder: "Nenhum arquivo selecionado",
      }
    },
    {
      type: "text",
      name: "code",
      label: "Código da Conta de Água",
      props: {
        placeholder: "Código",
        style: ""
      }
    },
    {
      type: "text",
      name: "documentation",
      label: "RG",
      props: {
        placeholder: "RG",
      }
    },
    {
      type: "text",
      name: "country",
      label: "País",
      props: {
        placeholder: "País",
      }
    },
    {
      type: "text",
      name: "state",
      label: "Estado",
      props: {
        placeholder: "Estado",
      }
    },
    {
      type: "text",
      name: "city",
      label: "Cidade",
      props: {
        placeholder: "Cidade",
      }
    },
    {
      type: "text",
      name: "neighborhood",
      label: "Bairro",
      props: {
        placeholder: "Bairro",
      }
    },
    {
      type: "text",
      name: "address",
      label: "Endereço",
      props: {
        placeholder: "Endereço",
      }
    },
    {
      type: "text",
      name: "zipcode",
      label: "CEP",
      props: {
        placeholder: "cep",
      }
    },
    {
      type: "text",
      name: "cellphone",
      label: "Celular",
      props: {
        placeholder: "Celular",
      }
    },
    {
      type: "text",
      name: "telephone",
      label: "Telefone",
      props: {
        placeholder: "Telefone",
      }
    },
    {
      type: "radio-group",
      name: "accept_terms",
      label: "Disponível",
      defaultValue: "true",
      props: {
      },
      options: [
        { label: "Sim", value: "true"},
        { label: "Não", value: "false"}
      ]
    },
  ]

  const { loading } = useQuery( queries.GET_DATA, {
    onCompleted: (response) => {
      let data
      
      if(response && response.waterRegisterByUser) 
        data = {
          ...response.waterRegisterByUser,
          accept_terms: response.waterRegisterByUser.accept_terms.toString(),
          media_user_id: response.waterRegisterByUser.mediaUser.id,
          media_documentation_frontside_id: response.waterRegisterByUser.mediaDocumentationFrontside.id,
          media_documentation_backside_id: response.waterRegisterByUser.mediaDocumentationBackside.id,
          media_water_account_id: response.waterRegisterByUser.mediaWaterAccount.id
        }

      setState({ 
        ...state,
        data
      })
    },
		onError: (error) => {
			console.log(error)
		}
  })

  const [state, setState] = useState({
    inputs, 
    schema, 
    initialState,
    data: {}
  })

  let mutation  
  if(state.data && state.data.id) { 
    mutation = { definition: mutations.UPDATE_DATA, name: 'updateWaterRegister' }
  } else {
    mutation = { definition: mutations.ADD_DATA, name: 'addWaterRegister' }
  }

  return (
    <Content title={'Cadastro de ligação de água'}>
      { loading ?
          <CircularProgress disableShrink />
        :
          <Form 
            title="Registro de Conta de Água" 
            description=""
            source={state.data} 
            inputs={state.inputs} 
            schema={state.schema} 
            mutation={mutation}
            callback={(data) => {
              if(data.success){
                window.location.reload()
              }
            }}
            initialState={state.initialState} />
      } 
    </Content>
  )
}