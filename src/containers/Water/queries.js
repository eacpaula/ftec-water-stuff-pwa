import { gql } from '@apollo/client'

const GET_DATA = gql`
  query {
    waterRegisterByUser {
      id
      code
      documentation
      mediaUser {
        id
        filename
        filename_original
      }
      mediaDocumentationFrontside {
        id
        filename
        filename_original
      }
      mediaDocumentationBackside {
        id
        filename
        filename_original
      }
      mediaWaterAccount { 
        id
        filename
        filename_original
      }
      accept_terms
      country
      state
      city
      neighborhood
      address
      zipcode
      cellphone
      telephone
    }
  }
`;

const queries = {
  GET_DATA
}

export default queries