import { gql } from '@apollo/client'

const ADD_DATA = gql`
  mutation AddWaterRegister(
    $code: String!
    $documentation: String!
    $media_user_id: Int!
    $media_documentation_frontside_id: Int!
    $media_documentation_backside_id: Int!
    $media_water_account_id: Int!
    $accept_terms: Boolean = true
    $available: Boolean = true
    $country: String!
    $state: String!
    $city: String!
    $neighborhood: String!
    $address: String!
    $zipcode: String!
    $cellphone: String!
    $telephone: String!
  ) {
    addWaterRegister(
      data: {
        code: $code
        documentation: $documentation
        media_user_id: $media_user_id
        media_documentation_frontside_id: $media_documentation_frontside_id
        media_documentation_backside_id: $media_documentation_backside_id
        media_water_account_id: $media_water_account_id
        accept_terms: $accept_terms
        available: $available
        country: $country
        state: $state
        city: $city
        neighborhood: $neighborhood
        address: $address
        zipcode: $zipcode
        cellphone: $cellphone
        telephone: $telephone
      }
    ) {
      id
    }
  }
`;

const UPDATE_DATA = gql`
  mutation UpdateWaterRegister(
    $id: Int
    $user_id: Int
    $code: String!
    $documentation: String!
    $media_user_id: Int!
    $media_documentation_frontside_id: Int!
    $media_documentation_backside_id: Int!
    $media_water_account_id: Int!
    $accept_terms: Boolean = true
    $available: Boolean = true
    $country: String!
    $state: String!
    $city: String!
    $neighborhood: String!
    $address: String!
    $zipcode: String!
    $cellphone: String!
    $telephone: String!
  ) {
    updateWaterRegister(
      data: {
        id: $id
        user_id: $user_id
        code: $code
        documentation: $documentation
        media_user_id: $media_user_id
        media_documentation_frontside_id: $media_documentation_frontside_id
        media_documentation_backside_id: $media_documentation_backside_id
        media_water_account_id: $media_water_account_id
        accept_terms: $accept_terms
        available: $available
        country: $country
        state: $state
        city: $city
        neighborhood: $neighborhood
        address: $address
        zipcode: $zipcode
        cellphone: $cellphone
        telephone: $telephone
      }
    ) {
      id
    }
  }
`;

const mutations = {
    ADD_DATA,
    UPDATE_DATA,
}

export default mutations