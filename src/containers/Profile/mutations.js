import { gql } from '@apollo/client'

const UPDATE_DATA = gql`
  mutation PartialUpdateProfile(
    $id: Int!
    $address: String!
    $cellphone: String!
    $city: String!
    $documentation_cpf: String!
    $documentation_rg: String!
    $fullname: String!
    $neighborhood: String!
    $zipcode: String!
  ) {
    partialUpdateProfile(
      data: {
        id: $id
        address: $address
        cellphone: $cellphone
        city: $city
        documentation_cpf: $documentation_cpf
        documentation_rg: $documentation_rg
        fullname: $fullname
        neighborhood: $neighborhood
        zipcode: $zipcode
      }
    ) {
      id
    }
  }
`;

const mutations = {
    UPDATE_DATA,
}

export default mutations
