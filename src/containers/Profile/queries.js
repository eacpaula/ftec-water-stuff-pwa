import { gql } from '@apollo/client'

const GET_DATA = gql`
  query GetProfile($id: Int!) {
    profile(id: $id){
      id
      address
      bank_account
      bank_account_digit
      bank_agency
      bank_code
      birthday
      cellphone
      city
      country
      documentation_cpf
      documentation_rg 
      fullname
      neighborhood
      state
      telephone
      zipcode
      allowAutomaticDebit
      createdBy {
        username
        email
      }
      updatedBy {
        username
        email
      }
      user {
        email
        username
      }
    }
  }
`;

const queries = {
  GET_DATA
}

export default queries