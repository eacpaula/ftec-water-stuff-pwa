import React, { useState } from 'react'
import { CircularProgress } from '@material-ui/core'
import { useQuery } from '@apollo/client'
import { object, string, number } from "yup"

import queries from './queries'
import mutations from './mutations'

import Content from '../../components/Content'
import Form from '../../components/Form'

import { getData } from '../../helpers/Storage'

export default function Profile() {
  const initialState = {
    fullname: "",
    // email: "",
    cellphone: "",
    address: "",
    zipcode: "",
    neighborhood: "",
    city: "",
    documentation_rg: "",
    documentation_cpf: ""
  }

  const schema = object().shape({
    fullname: string().required("O nome do usuário é obrigatório!"),
    // email: string().required("O e-mail do usuário é obrigatório!"),
    cellphone: string().required("O telefone do usuário é obrigatório!"),
    address: string().required("A rua do usuário é obrigatória!"),
    zipcode: string().required("O CEP do usuário é obrigatório!"),
    neighborhood: string().required("O bairro do usuário é obrigatório!"),
    city: string().required("A cidade do usuário é obrigatória!"),
    documentation_rg: string().required("O RG do usuário é obrigatório!"),
    documentation_cpf: string().required("O CPF do usuário é obrigatório!")
	})

  const inputs = [
    {
      type: "hidden",
      name: "id",
    },
    {
      type: "text",
      name: "fullname",
      label: "Nome completo",
      props: {
        placeholder: "Nome do usuário",
        style: ""
      }
    },
    // {
    //   type: "text",
    //   name: "email",
    //   label: "E-mail",
    //   props: {
    //     placeholder: "E-mail do usuário",
    //     style: ""
    //   }
    // },
    {
      type: "text",
      name: "cellphone",
      label: "Telefone",
      props: {
        placeholder: "Telefone do usuário",
        style: ""
      }
    },
    {
      type: "text",
      name: "address",
      label: "Rua",
      props: {
        placeholder: "Rua do usuário",
        style: ""
      }
    },
    {
      type: "text",
      name: "zipcode",
      label: "CEP",
      props: {
        placeholder: "CEP do usuário",
        style: ""
      }
    },
    {
      type: "text",
      name: "neighborhood",
      label: "Bairro",
      props: {
        placeholder: "Bairro do usuário",
        style: ""
      }
    },
    {
      type: "text",
      name: "city",
      label: "Cidade",
      props: {
        placeholder: "Cidade do usuário",
        style: ""
      }
    },
    {
      type: "text",
      name: "documentation_rg",
      label: "RG",
      props: {
        placeholder: "RG do usuário",
        style: ""
      }
    },
    {
      type: "text",
      name: "documentation_cpf",
      label: "CPF",
      props: {
        placeholder: "CPF do usuário",
        style: ""
      }
    },
  ]

  const storage = getData()

  const { loading } = useQuery( queries.GET_DATA, {
    variables: { id: storage.userId },
    onCompleted: (response) => {
      var data

      if(response && response.profile) {
        const info = response.profile;

        data = {
          ...info,
          id: parseInt(info.id),
          email: info.user.email
        }
      } else {
        data = {
          id: 0,
        }
      }

      setState({
        ...state,
        data
      })
    },
		onError: (error) => {
			console.log(error)
		}
  })

  const [state, setState] = useState({
    inputs,
    schema,
    initialState,
    data: {}
  })

  const mutation = { definition: mutations.UPDATE_DATA, name: 'partialUpdateProfile' }

  return (
    <Content title={'Perfil do usuário'}>
      { loading ?
          <CircularProgress disableShrink />
        :
          <Form
            title="Perfil do usuário"
            description=""
            source={state.data}
            inputs={state.inputs}
            schema={state.schema}
            mutation={mutation}
            callback={(data) => {
              if(data.success){
                window.location.reload()
              }
            }}
            initialState={state.initialState} />
      }
    </Content>
  )
}