import React, { useState, useEffect } from 'react'
import { CircularProgress } from '@material-ui/core'
import { useLazyQuery } from '@apollo/client'
import { object, string, number } from "yup"

import queries from './queries'
import mutations from './mutations'

import Content from '../../components/Content'
import Form from '../../components/Form'

export default function WaterReinstallation() {
  const [loading, setLoading] = useState([]);

  const initialState = {
    code: "",
    address: "",
    message: ""
  }

  const schema = object().shape({
    code: string(),
    address: string(),
    message: string().required("A mensagem é obrigatória!"),
    water_register_id: number()
	})

  const inputs = [
    {
      type: "text",
      name: "code",
      label: "Código da Conta de Água",
      readOnly: true,
      props: {
        placeholder: "Código",
        style: ""
      }
    },
    {
      type: "text",
      name: "address",
      label: "Endereço",
      readOnly: true,
      props: {
        placeholder: "Endereço",
      }
    },
    {
      type: "text",
      name: "message",
      label: "Mensagem",
      props: {
        placeholder: "Mensagem",
      }
    },
    {
      type: "hidden",
      name: "water_register_id"
    },
  ]

  const [load] = useLazyQuery(queries.GET_DATA, {
    fetchPolicy: 'no-cache',
    onCompleted: (response) => {
      var data
      
      if(response && response.waterRegisterByUser) {
        const info = response.waterRegisterByUser;
        
        data = {
          ...info,
          address: `${info.address} - ${info.neighborhood} - ${info.city} - ${info.state} - ${info.zipcode}`,
          fullname: '',
          email: info.createdBy.email,
          water_register_id: info.id,
          subject_id: 1
        }
      }

      setState({ 
        ...state,
        data
      })

      setLoading(false)
    },
		onError: (error) => {
			console.log(error)
      setLoading(false)
		}
  })

  const [state, setState] = useState({
    inputs, 
    schema, 
    initialState,
    data: null,
  })

  const mutation = { definition: mutations.ADD_DATA, name: 'addWaterRegisterContact' }

  useEffect(() => {
		(async () => {
      if(!state.data){
        setLoading(true)
			  load()
      }
    })()
    // eslint-disable-next-line react-hooks/exhaustive-deps
	}, [state])

  return (
    <Content title={'Desligar instalação de água'}>
      { loading ?
          <CircularProgress disableShrink />
        :
          <Form 
            title="Desligar instalação de água" 
            description=""
            source={state.data} 
            inputs={state.inputs} 
            schema={state.schema} 
            mutation={mutation}
            callback={(data) => {
              if(data.success){
                window.location.reload()
              }
            }}
            initialState={state.initialState} />
      } 
    </Content>
  )
}