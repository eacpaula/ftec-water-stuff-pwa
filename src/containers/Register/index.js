import * as React from 'react'
import { Box, Button, Container, CssBaseline, TextField, Typography, InputLabel, MenuItem, FormControl, Select, Checkbox } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import Logo from '../../logo.svg'
import Footer from '../../components/Footer'
import Link from '@material-ui/core/Link';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  logo: {
    margin: '-50px'
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  selectContainer: {
    display: 'flex',
    marginTop: '16px',
    marginBottom: '8px',
  },
  containerActionButton: {
    textAlign: 'center',
  },
  ActionButton: {
    marginRight: '1rem',
  },
}))

export default function Register() {
  const classes = useStyles()
  return (
    <Container component={'main'} maxWidth={'xs'}>
      <CssBaseline />
      <div className={classes.paper}>
        <div className={classes.logo}>
          <img src={Logo} alt="logotipo" style={{ width: "300px"}} />
        </div>
        <Typography component={'h1'} variant={'h5'}>
          Cadastro
        </Typography>
        <form className={classes.form}>
          <TextField
            variant={'outlined'}
            margin={'normal'}
            required
            fullWidth
            id={'fullname'}
            label={'Nome completo'}
            name={'fullname'}
            type={'text'}
            autoFocus
          />
          <TextField
            variant={'outlined'}
            margin={'normal'}
            required
            fullWidth
            id={'email'}
            label={'E-mail'}
            name={'email'}
            type={'email'}
            autoFocus
          />
          <TextField
            variant={'outlined'}
            margin={'normal'}
            required
            fullWidth
            name={'password'}
            label={'Senha'}
            type={'password'}
            id={'password'}
            autoComplete={'current-password'}
          />
          <TextField
            variant={'outlined'}
            margin={'normal'}
            required
            fullWidth
            name={'password-confirm'}
            label={'Repetir senha'}
            type={'password-confirm'}
            id={'password'}
            autoComplete={'current-password'}
          />
          <TextField
            id="date"
            label="Data de nascimento"
            type="date"
            className={classes.textField}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <div>
            <FormControl className={classes.selectContainer}>
              <InputLabel id="localization">Cidade/UF</InputLabel>
              <Select
                labelId="localization"
                id="localization-select"
                variant="outlined"
              >
                <MenuItem value={10}>Caxias do Sul/RS</MenuItem>
                <MenuItem value={20}>Porto alegre/RS</MenuItem>
                <MenuItem value={30}>São paulo/SP</MenuItem>
              </Select>
            </FormControl>
          </div>
          <div className={classes.containerButton}>
            <Checkbox />Concordo com os termos de uso e serviço
          </div>
          <div className={classes.containerActionButton}>
            <Button
              type={'submit'}
              variant={'contained'}
              color={'primary'}
              size={'large'}
              className={classes.ActionButton}
            >
              Cadastrar
            </Button>
          </div>
        </form>
        <Link color="primary" href="login">
          Login
        </Link>
      </div>
      <Box mt={8}>
        <Footer />
      </Box>
    </Container>
  )
}