import React from 'react';
import { Controller } from "react-hook-form"
import UploadFile from "../../../UploadFile"

import './style.css';

const Upload = ({id, name, label, placeholder, mediaData, errors, control, register}) => {
    return <div className='form__input__upload'>
        <label htmlFor={name}>{label}</label>
        <Controller
            control={control}
            id={id}
            name={name}
            render={({ field: { onChange, value, ref } }) => (
                <UploadFile 
                    name={name}
                    inputRef={ref}
                    value={value}
                    mediaData={mediaData}
                    handleOnChange={onChange}
                    callback={(success, data) => {}}
                />
            )}
        />
    </div>
}

export default Upload