import React, { Fragment } from "react";
import { 
    Button,
    DialogActions as DialogActionsMUI, 
    CircularProgress
} from "@material-ui/core"

import './style.css';

const DialogActions = ({status, handleClose, handleSubmit, loading}) => {
    return (
        <DialogActionsMUI>
            { status === 'success' ? 
                <Button onClick={handleClose} color="primary">
                Fechar
                </Button>
            :
                <Fragment>
                    <Button onClick={handleClose} color="primary">
                        Cancelar
                    </Button>
                    <Button onClick={handleSubmit} color="primary">
                        {loading && <CircularProgress disableShrink />}
                        Salvar
                    </Button>
                </Fragment>
            }
        </DialogActionsMUI>
    )
}

export default DialogActions