import * as React from 'react'
import { ListItem, ListItemIcon, ListItemText } from '@material-ui/core'
import {
  ArtTrack,
  AspectRatio,
  Dashboard,
  Email,
  People,
  QuestionAnswer,
  Work,
  ViewCarousel
} from '@material-ui/icons'
import { Link } from 'react-router-dom'

function ListItemLink(props) {
  const { icon, primary, to } = props;

  const CustomLink = props => <Link to={to} {...props} />;

  return (
    <li>
      <ListItem button component={CustomLink}>
        <ListItemIcon>{icon}</ListItemIcon>
        <ListItemText primary={primary} />
      </ListItem>
    </li>
  )
}

export default function MenuItems() {
  return (
    <div>
      <ListItemLink
        icon={<Dashboard />}
        primary={'Dashboard'}
        to={'/dashboard'}
      />

      <ListItemLink
        icon={<AspectRatio />}
        primary={'Instalação de água'}
        to={'/water'}
      />
      <ListItemLink
        icon={<ViewCarousel />}
        primary={'Religar instalação'}
        to={'/water-reinstalation'}
      />
      <ListItemLink
        icon={<People />}
        primary={'Desligar instalação'}
        to={'/water-unstallation'}
      />
      <ListItemLink
        icon={<Email />}
        primary={'Leituras'}
        to={'/water-reading'}
      />
      <ListItemLink
        icon={<ArtTrack />}
        primary={'Histórico de faturas'}
        to={'/invoice'}
      />
      <ListItemLink
        icon={<Work />}
        primary={'Pagamento online'}
        to={'/invoice-online-payment'}
      />
      <ListItemLink
        icon={<QuestionAnswer />}
        primary={'Débito automático'}
        to={'/invoice-automatic-debit'}
      />
      <ListItemLink
        icon={<QuestionAnswer />}
        primary={'Avaliações'}
        to={'/ratings'}
      />
      <ListItemLink
        icon={<QuestionAnswer />}
        primary={'Denúncias'}
        to={'/complaints'}
      />
      <ListItemLink
        icon={<QuestionAnswer />}
        primary={'Notas públicas'}
        to={'/public-notes'}
      />
    </div>
  )
}